---
sh: touch src/services/index.ts
to: src/services/<%= Name %>Service.ts
---
import BaseService from '@services/BaseService';
import { <%= Name %>Snapshot } from '@stores/<%= Name %>Store/<%= Name %>Model';

class <%= Name %>Service extends BaseService<<%= Name %>Snapshot> {}

export default new <%= Name %>Service('/<%= name.toLowerCase() %>s');