---
inject: true
prepend: true
skip_if: <%= Name %>Service
to: src/services/index.ts
---
export { default as <%= Name %>Service } from './<%= Name %>Service';