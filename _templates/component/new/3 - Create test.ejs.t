---
to: src/components/<%= type.toLowerCase() %>/<%= Name %>/<%= Name %>.test.ts
---
import React from 'react';
import renderer from 'react-test-renderer';
import <%= Name %> from './<%= Name %>';

it('<%= Name %>: default', () => {
  const component = renderer.create(<<%= Name %> />)
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

