---
inject: true
prepend: true
skip_if: <%= Name %>
to: src/components/<%= type.toLowerCase() %>/index.ts
---
export { default as <%= Name %> } from './<%= Name %>/<%= Name %>';