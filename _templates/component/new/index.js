module.exports = [
    {
        type: 'input',
        name: 'name',
        message: "What's the name of the component?"
    },

    {
        type: 'select',
        name: 'type',
        choices: ["UI", "Layout", "Containers"],
        message: "What's the type of the component?"
    }
]