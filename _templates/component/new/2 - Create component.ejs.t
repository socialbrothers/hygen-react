---
sh: touch src/components/index.ts && touch src/components/<%= type.toLowerCase() %>/index.ts
to: src/components/<%= type.toLowerCase() %>/<%= Name %>/<%= Name %>.ts
---
import React from 'react';
import { StyleSheet, View } from 'react-native';

import Props from './<%= Name %>.props';

const <%= Name %> = ({ ...otherProps }: Props) => {
  return (
    <View></View>
  );
};

const styles = StyleSheet.create({});

export default <%= Name %>;
