---
inject: true
prepend: true
skip_if: <%= h.capitalize(type) %>
to: src/components/index.ts
---
export * from './<%= h.capitalize(type) %>';