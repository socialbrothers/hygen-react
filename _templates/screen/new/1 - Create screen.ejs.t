---
sh: touch src/screens/index.ts && touch src/screens/<%= navigation %>/index.ts
to: src/screens/<%= h.capitalize(navigation) %>/<%= Name %>Screen.ts
---
import React from 'react';
import { StyleSheet, View } from 'react-native';

type Props = {
  navigation: StackNavigationProp<<%= h.capitalize(navigation) %>StackParamListType, '<%= h.capitalize(navigation) %>StackParamListType'>;
};

const <%= Name %>Screen = ({ navigation }: Props) => {

  return (
    <View></View>
  );
};

const styles = StyleSheet.create({});

export default <%= Name %>Screen;