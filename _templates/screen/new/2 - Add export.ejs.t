---
inject: true
prepend: true
skip_if: <%= Name %>Screen
to: src/screens/<%= h.capitalize(navigation) %>/index.ts
---
export { default as <%= Name %>Screen } from './<%= Name %>Screen';