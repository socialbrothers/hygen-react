module.exports = [
    {
        type: 'input',
        name: 'name',
        message: "What's the name of the screen?"
    },

    {
        type: 'input',
        name: 'navigation',
        message: "What's the name of the navigation?"
    }
]