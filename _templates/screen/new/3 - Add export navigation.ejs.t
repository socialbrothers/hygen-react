---
inject: true
prepend: true
skip_if: <%= h.capitalize(navigation) %>
to: src/screens/index.ts
---
export * from './<%= h.capitalize(navigation) %>';