---
sh: touch src/stores/index.ts
to: src/stores/<%= Name %>Store/<%= Name %>Store.ts
---
import { types, Instance, SnapshotIn } from 'mobx-state-tree';

import <%= Name %>Service from '@services/<%= Name %>Service';
import <%= Name %>Model from '@stores/<%= Name %>Store/<%= Name %>Model';

const <%= Name %>Store = types
  .model('<%= Name %>StoreModel', {})
  .actions((self) => ({}));

export interface <%= Name %>StoreSnapshot extends SnapshotIn<typeof <%= Name %>Store> {}
export interface <%= Name %>StoreInstance extends Instance<typeof <%= Name %>Store> {}

export default <%= Name %>Store;
