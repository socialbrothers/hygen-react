---
inject: true
prepend: true
skip_if: <%= Name %>StoreSnapshot
to: src/stores/index.ts
---
export { default as <%= Name %>StoreSnapshot } from './<%= Name %>Store/<%= Name %>Store';