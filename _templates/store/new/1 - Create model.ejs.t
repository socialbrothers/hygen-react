---
to: src/stores/<%= Name %>Store/<%= Name %>Model.ts
---
import { types, Instance, SnapshotIn } from 'mobx-state-tree';

const <%= Name %>Model = types
  .model('<%= Name %>Model', {})
  .views((self) => ({}));

export interface <%= Name %>Instance extends Instance<typeof <%= Name %>Model> {}
export interface <%= Name %>Snapshot extends SnapshotIn<typeof <%= Name %>Model> {}

export default <%= Name %>Model;
