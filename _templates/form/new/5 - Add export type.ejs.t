---
inject: true
prepend: true
skip_if: Forms
to: src/components/index.ts
---
export * from './Forms';