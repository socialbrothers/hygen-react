---
to: src/components/Forms/<%= Name %>/<%= Name %>Form.props.ts
---
import { Form } from '@constants/TYPES';

export default interface <%= Name %>FormProps extends Form<<%= Name %>FormValues> {}

export interface <%= Name %>FormValues {}


