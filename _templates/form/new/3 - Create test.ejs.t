---
to: src/components/Forms/<%= Name %>/<%= Name %>Form.test.ts
---
import React from 'react';
import renderer from 'react-test-renderer';
import <%= Name %>Form from './<%= Name %>Form';

it('<%= Name %>Form: default', () => {
  const component = renderer.create(<<%= Name %>Form />)
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

