---
inject: true
prepend: true
skip_if: <%= Name %>Form
to: src/components/Forms/index.ts
---
export { default as <%= Name %>Form } from './<%= Name %>Form';