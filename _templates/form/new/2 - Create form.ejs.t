---
sh: touch src/components/index.ts && touch src/components/Forms/index.ts
to: src/components/Forms/<%= Name %>/<%= Name %>Form.ts
---
import React, { View } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import Props from './<%= Name %>Form.props';

const <%= Name %>Form = ({ initialValues, onSubmit, loading, children }: Props) => {
  const validationScheme = Yup.object().shape({});

  return (
    <Formik
      validateOnMount
      validationSchema={validationScheme}
      onSubmit={onSubmit}
      initialValues={initialValues}>
      {({ handleChange, errors, values, touched, handleBlur, handleSubmit }) => {
        return (
          <View></View>
        );
      }}
    </Formik>
  );
};

export default <%= Name %>Form;