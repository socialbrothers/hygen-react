# React CLI
🔥 The hottest CLI for React and React Native projects! Generators for components, screens, and more. 🔥

## Why React CLI?
* Easily spin up a new React (Native) app with best practices built-in
* Many easy to use generators that can generate the needed files for you
* No runtime! This is a developer tool only, not a library you have to depend on and figure out how to upgrade
* Works on macOS, Windows, and Linux because not all React Native developers are on one platform
* Saves an average of two weeks on your React development

## ⬇️ Install
First, make sure you're set up for React or React Native

then...

You have to install Hygen. This is a fresh take on code generators that’s super suitable for React projects. It lets you build new generators in a breeze and use them immediately with zero configuration and zero friction, while keeping your generators project-local.

**On macOS and Homebrew:**
```bash
$ brew tap jondot/tap
$ brew install hygen
```

**On Windows or Linux:**
```bash
$ npm i -g hygen
```